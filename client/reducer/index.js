import { combineReducers } from 'redux';
import auth from './auth.reducer';
import lobby from './lobby.reducer';

export default combineReducers({
  auth,
  lobby,
});
