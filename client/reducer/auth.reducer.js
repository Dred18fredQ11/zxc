const initialState = {
  user: null,
  fetchingUserStatus: 'idle',
};

function authReducer(state = initialState, { type, payload }) {
  switch (type) {
    case 'auth/dataFetching': {
      return { ...state, fetchingUserStatus: 'loading' };
    }
    case 'auth/dataFetchingSuccess': {
      const { user } = payload;
      return { ...state, user, fetchingUserStatus: 'succeeded'};
    }
    case 'auth/dataFetchingError': {
      localStorage.removeItem('jwt');
      return { ...state, fetchingUserStatus: 'failed' };
    }
    case 'auth/logout': {
      localStorage.removeItem('jwt');
      return { ...state, user: null, fetchingUserStatus: 'idle' };
    }
    default: {
      return state;
    }
  }
}

export default authReducer;
