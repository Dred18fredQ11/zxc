const initialState = {
  fetchingLobbyStatus: 'idle',
  owner: null,
  opponent: null,
  lobby: null,
};

function lobbyReducer(state = initialState, { type, payload }) {
  switch (type) {
    case 'lobby/creating/request': {
      return {
        ...state,
        fetchingLobbyStatus: 'loading',
        ownerId: null,
        player: null,
        lobby: null,
      };
    }
    case 'lobby/creating/success': {
      const lobby = {
        id: payload.lobby.id,
        name: payload.lobby.name,
        status: payload.lobby.status,
        isMangoBanned: payload.lobby.isMangoBanned,
        isCloakBanned: payload.lobby.isCloakBanned,
        isHatBanned: payload.lobby.isHatBanned,
        isRaindropsBanned: payload.lobby.isRaindropsBanned,
        isStickBanned: payload.lobby.isStickBanned,
        isBottleBanned: payload.lobby.isBottleBanned,
      };
      return {
        ...state,
        fetchingLobbyStatus: 'success',
        owner: {
          ...payload.owner,
          isReady: false,
          ownerDotaId: '',
        },
        opponent: null,
        lobby,
      };
    }
    case 'lobby/creating/error': {
      return {
        ...state,
        fetchingLobbyStatus: 'error',
        owner: null,
        opponent: null,
        lobby: null,
      };
    }
    case 'lobby/joining/request': {
      return {
        ...state,
        fetchingLobbyStatus: 'loading',
        owner: null,
        opponent: null,
        lobby: null,
      };
    }
    case 'lobby/joining/success': {
      const lobby = {
        id: payload.lobby.id,
        name: payload.lobby.name,
        status: payload.lobby.status,
        isMangoBanned: payload.lobby.isMangoBanned,
        isCloakBanned: payload.lobby.isCloakBanned,
        isHatBanned: payload.lobby.isHatBanned,
        isRaindropsBanned: payload.lobby.isRaindropsBanned,
        isStickBanned: payload.lobby.isStickBanned,
        isBottleBanned: payload.lobby.isBottleBanned,
      };
      const owner = {
        id: payload.lobby.owner.id,
        nickname: payload.lobby.owner.nickname,
        avatar: payload.lobby.owner.avatar,
        isReady: payload.lobby.isOwnerReady,
        dotaId: payload.lobby.ownerDotaId,
      };
      const opponent = {
        ...payload.opponent,
        isReady: false,
        dotaId: '',
      };
      return {
        ...state,
        fetchingLobbyStatus: 'success',
        lobby,
        owner,
        opponent,
      };
    }
    case 'lobby/joining/error': {
      return {
        ...state,
        fetchingLobbyStatus: 'error',
        owner: null,
        opponent: null,
        lobby: null,
      };
    }
    case 'lobby/leave': {
      return {
        ...state,
        fetchingLobbyStatus: 'idle',
        owner: null,
        opponent: null,
        lobby: null,
      };
    }
    case 'lobby/ready': {
      return {
        ...state,
        [payload.playerKey]: {
          ...state[payload.playerKey],
          isReady: true,
        },
      };
    }
    case 'lobby/unready': {
      return {
        ...state,
        [payload.playerKey]: {
          ...state[payload.playerKey],
          isReady: false,
        },
      };
    }
    case 'lobby/setDotaId': {
      return {
        ...state,
        [payload.playerKey]: {
          ...state[payload.playerKey],
          dotaId: payload.dotaId,
        },
      };
    }
    case 'lobby/joined': {
      return {
        ...state,
        opponent: {
          ...payload.opponent,
          isReady: false,
          dotaId: '',
        },
      };
    }
    case 'lobby/leaved': {
      return {
        ...state,
        opponent: null,
      };
    }
    case 'lobby/canceled': {
      return {
        ...state,
        fetchingLobbyStatus: 'idle',
        owner: null,
        opponent: null,
        lobby: null,
      };
    }
    case 'lobby/started': {
      return {
        ...state,
        lobby: {
          ...state.lobby,
          isStarted: true,
        },
      };
    }
    default: {
      return state;
    }
  }
}

export default lobbyReducer;
