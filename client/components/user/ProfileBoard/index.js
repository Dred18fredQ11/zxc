import { useSelector } from 'react-redux';
import classnames from 'classnames';
import styles from './styles.module.css';

export default function ProfileBoard({ className }) {
  const user = useSelector(state => state.auth.user);
  return (
    <div className={classnames(styles.wrapper, className)}>
      <span className={styles.nickname}>{user.nickname}</span>
      <img
        className={styles.avatar}
        src={user.avatar}
        alt="User"
      />
    </div>
  );
}
