import { useSelector } from 'react-redux';
import classnames from 'classnames';
import styles from './styles.module.css';

export default function BalanceBoard({ className }) {
  const balance = useSelector(state => state.auth.user.balance);
  return (
    <div className={classnames(styles.wrapper, className)}>
      <span className={styles.balance}>Баланс:</span>
      <span className={styles.amount}>{parseInt(balance ?? 0).toFixed(2)}</span>
      <span className={styles.currency}>RUB</span>
    </div>
  );
}
