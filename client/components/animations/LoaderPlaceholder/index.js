import Loader from 'react-loader-spinner';
import classnames from 'classnames';
import styles from './styles.module.css';

export default function LoaderPlaceholder({
  className,
  color = '#ffffff',
  width = 100,
  height = 100,
}) {
  return (
    <div className={classnames(styles.wrapper, className)}>
      <Loader type="ThreeDots" color={color} width={width} height={height} />
    </div>
  );
}
