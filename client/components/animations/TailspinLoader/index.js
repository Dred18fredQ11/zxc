import Loader from 'react-loader-spinner';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Tailspin loader component with animation.
 * @param {{
 *  color?: string;
 *  width?: number;
 *  height?: number;
 *  timeout?: number;
 *  className?: string;
 * }} props
 */
function TailspinLoader({
  color = '#8E8E93',
  width = 25,
  height = 25,
  className,
}) {
  // Classes CSS
  const wrapperClass = classnames(styles.wrapper, className);

  return (
    <div className={wrapperClass}>
      <Loader
        type="TailSpin"
        color={color}
        height={width}
        width={height}
      />
    </div>
  );
}

export default TailspinLoader;
