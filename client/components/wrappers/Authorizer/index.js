import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import socket from 'socket';

/**
 * Requests user's data.
 * @param {{ children?: JSX.Element }} props
 */
function Authorizer({ children }) {
  const dispatch = useDispatch();

  // Socket Event Handlers
  function emitUserData(token) {
    socket.auth = { ...socket.auth, token };
    dispatch({ type: 'auth/dataFetching' });
    socket.emit('user:data');
  }

  function handleUserDataSuccess(response) {
    const user = response.data.user;
    dispatch({ type: 'auth/dataFetchingSuccess', payload: { user } });
  }

  function handleUSerDataError(response) {
    console.error(response);
    dispatch({ type: 'auth/dataFetchingError' });
  }

  // Effect Hooks
  useEffect(() => {
    socket.on('user:data:success', handleUserDataSuccess);
    socket.on('user:data:error', handleUSerDataError);
    const token = localStorage.getItem('jwt');
    if (token) emitUserData(token);
    return () => {
      socket.off('user:data:success', handleUserDataSuccess);
      socket.off('user:data:error', handleUSerDataError);
    };
  }, []);

  // Render
  return children;
}

export default Authorizer;
