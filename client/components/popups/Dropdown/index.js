import { useState, useRef } from 'react';
import { CSSTransition } from 'react-transition-group';
import { useOutsideClick } from 'hooks/layout.hooks';
import classnames from 'classnames';
import styles from './styles.module.css';

export default function DropdownPopup({ Dropdown, Toggler, className }) {
  const [isOpen, setIsOpen] = useState(false);
  const wrapperRef = useRef(null);
  useOutsideClick(wrapperRef, () => setIsOpen(false));
  return (
    <div className={classnames(styles.wrapper, className)} ref={wrapperRef}>
      <Toggler onClick={() => setIsOpen(!isOpen)} isOpen={isOpen} />
      <CSSTransition
        in={isOpen}
        timeout={300}
        classNames={{
          enter: styles.dropdownEnter,
          enterActive: styles.dropdownEnterActive,
          enterDone: styles.dropdownEnterDone,
          exit: styles.dropdownExit,
        }}
      >
        <Dropdown className={styles.dropdown} isOpen={isOpen} />
      </CSSTransition>
    </div>
  );
}
