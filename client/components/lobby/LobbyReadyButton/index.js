import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Button from 'components/controllers/Button';
import socket from 'socket';
import classnames from 'classnames';
import styles from './styles.module.css';

function LobbyReadyButton({ playerKey, className }) {
  const dispatch = useDispatch();

  // Selector Hooks
  const playerId = useSelector(state => state.lobby[playerKey].id);
  const isUser = useSelector(state => {
    return state.auth.user.id === state.lobby[playerKey].id;
  });
  const isReady = useSelector(state => state.lobby[playerKey].isReady);
  const lobby = useSelector(state => state.lobby.lobby);
  const dotaId = useSelector(state => state.lobby[playerKey].dotaId);
  const isStarted = useSelector(state => state.lobby.lobby.isStarted);

  // Document Event Handlers
  function handleReadyClick() {
    if (!isUser || isStarted) return;
    if (!dotaId) {
      return console.error('error'); 
    }
    const socketEvent = !isReady ? 'lobby:ready' : 'lobby:unready';
    socket.emit(socketEvent, { lobbyId: lobby.id });
  }

  // Socket Event Handlers
  function handleLobbyReadySuccess(response) {
    if (response.data.userId === playerId) {
      dispatch({ type: 'lobby/ready', payload: { playerKey } });
    }
  }

  function handleLobbyReadyError(response) {
    console.error(response);
  }

  function handleLobbyUnreadySuccess(response) {
    if (response.data.userId === playerId) {
      dispatch({ type: 'lobby/unready', payload: { playerKey } });
    }
  }

  function handleLobbyUnreadyError(response) {
    console.error(response);
  }

  // Effect Hooks
  useEffect(() => {
    socket.on('lobby:ready:success', handleLobbyReadySuccess);
    socket.on('lobby:ready:error', handleLobbyReadyError);
    socket.on('lobby:unready:success', handleLobbyUnreadySuccess);
    socket.on('lobby:unready:error', handleLobbyUnreadyError);
    return () => {
      socket.off('lobby:ready:success', handleLobbyReadySuccess);
      socket.off('lobby:ready:error', handleLobbyReadyError);
      socket.off('lobby:unready:success', handleLobbyUnreadySuccess);
      socket.off('lobby:unready:error', handleLobbyUnreadyError);
    };
  }, []);

  // Render
  if (!isUser) {
    return (
      <div
        className={classnames(
          styles.Button,
          styles.MockButton,
          isReady && styles.MockButtonActive,
          className,
        )}
      >
        <span>Готов</span>
      </div>
    );
  }

  return (
    <Button
      className={classnames(styles.Button, className)}
      variant="green"
      isActive={isReady}
      onClick={handleReadyClick}
    >
      Готов
    </Button>
  );
}

export default LobbyReadyButton;
