import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import Image from 'next/image';
import socket from 'socket';
import classnames from 'classnames';
import styles from './styles.module.css';

const defaultItemList = [
  {
    id: 'bottle',
    name: 'Bottle',
    img: '/assets/bottle.svg',
    isBanned: false,
  },
  {
    id: 'cloak',
    name: 'Cloak',
    img: '/assets/cloak.svg',
    isBanned: false,
  },
  {
    id: 'hat',
    name: 'Hat',
    img: '/assets/hat.svg',
    isBanned: false,
  },
  {
    id: 'mango',
    name: 'Mango',
    img: '/assets/mango.svg',
    isBanned: false,
  },
  {
    id: 'raindrops',
    name: 'Raindrops',
    img: '/assets/raindrops.svg',
    isBanned: false,
  },
  {
    id: 'stick',
    name: 'Stick',
    img: '/assets/stick.svg',
    isBanned: false,
  },
];

const namePropDict = {
  mango: 'isMangoBanned',
  cloak: 'isCloakBanned',
  hat: 'isHatBanned',
  raindrops: 'isRaindropsBanned',
  stick: 'isStickBanned',
  bottle: 'isBottleBanned',
};

/**
 * Contains list of dota2 items in lobby.
 * @param {{ className?: string }} props
 */
function ItemListContainer({ className }) {
  // Selector Hooks
  const lobby = useSelector(state => state.lobby.lobby);
  const isOwner = useSelector(state => {
    return state.auth.user.id === state.lobby.owner.id;
  });
  const isReady = useSelector(state => state.lobby.owner.isReady);

  // State Hooks
  const [itemList, setItemList] = useState(
    defaultItemList.map(item => {
      return { ...item, isBanned: lobby[namePropDict[item.id]] };
    }),
  );

  // Document Event Handlers
  function handleBanItemClick(itemName, isBanned) {
    return () => {
      if (!isOwner || isReady) return;
      const socketEvent = !isBanned ? 'lobby:item:ban' : 'lobby:item:unban';
      socket.emit(socketEvent, { lobbyId: lobby.id, itemName });
    };
  }

  // Socket Event Handlers
  function handleLobbyItemBanned(response) {
    setItemList(oldItemList => {
      return oldItemList.map(item => {
        if (item.id === response.data.itemName) {
          return { ...item, isBanned: true };
        }
        return item;
      });
    });
  }

  function handleLobbyItemUnbanned(response) {
    setItemList(oldItemList => {
      return oldItemList.map(item => {
        if (item.id === response.data.itemName) {
          return { ...item, isBanned: false };
        }
        return item;
      });
    });
  }

  // Effect Hooks
  useEffect(() => {
    socket.on('lobby:item:banned', handleLobbyItemBanned);
    socket.on('lobby:item:unbanned', handleLobbyItemUnbanned);
    return () => {
      socket.off('lobby:item:banned', handleLobbyItemBanned);
      socket.off('lobby:item:unbanned', handleLobbyItemUnbanned);
    };
  }, []);

  // Render
  return (
    <div className={classnames(styles.Container, className)}>
      <div className={styles.Header}>Запрещенные предметы:</div>
      <div className={styles.Body}>
        <div className={styles.Grid}>
          {itemList.map(item => (
            <button
              key={item.id}
              className={classnames(
                styles.Item,
                item.isBanned && styles.ItemBanned,
              )}
              onClick={handleBanItemClick(item.id, item.isBanned)}
            >
              <Image src={item.img} width={70} height={50} alt={item.name} />
              {item.isBanned && (
                <div className={styles.ItemBannedCross}>
                  <Image
                    src="/assets/red-cross.svg"
                    width={40}
                    height={40}
                    alt="Banned"
                  />
                </div>
              )}
            </button>
          ))}
        </div>
      </div>
    </div>
  );
}

export default ItemListContainer;
