import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSelector, useDispatch } from 'react-redux';
import Input from 'components/controllers/Input';
import Button from 'components/controllers/Button';
import socket from 'socket';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Component for creating form.
 * @param {{ className?: string }} props
 */
function LobbyCreator({ className }) {
  const router = useRouter();
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.user);

  // State Hooks
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState('');
  const [nameError, setNameError] = useState('');
  const [sum, setSum] = useState('50.00');

  // Document Event Handlers
  async function handleSubmit(e) {
    e.preventDefault();
    setNameError('');
    setIsLoading(true);

    if (name.length > 32) {
      setNameError('Не длинее 32 символов');
      return setIsLoading(false);
    }

    dispatch({ type: 'lobby/creating/request' });
    socket.emit('lobby:create', { name, sum: parseFloat(sum) });
  }

  // Socket Event Handlers
  function handleCreateSuccess(response) {
    setIsLoading(false);
    const lobby = response.data.lobby;
    dispatch({
      type: 'lobby/creating/success',
      payload: { lobby, owner: user },
    });
    router.push(`/lobby/${response.data.lobby.id}`);
  }

  function handleCreateError(response) {
    console.error(response);
    dispatch({ type: 'lobby/creating/error' });
    setIsLoading(false);
  }

  // Effect Hooks
  useEffect(() => {
    socket.on('lobby:create:success', handleCreateSuccess);
    socket.on('lobby:create:error', handleCreateError);
    return () => {
      socket.off('lobby:create:success', handleCreateSuccess);
      socket.off('lobby:create:error', handleCreateError);
    };
  }, []);

  // Render
  return (
    <div className={classnames(styles.Container, className)}>
      <div className={styles.Header}>Создание игры:</div>
      <form className={styles.Body} onSubmit={handleSubmit}>
        <Input
          name="name"
          label="Название игры:"
          value={name}
          errorMessage={nameError}
          onChange={e => setName(e.target.value)}
          disabled={isLoading}
        />
        <Input
          name="sum"
          type="number"
          label="Сумма (в рублях):"
          min={0}
          value={sum}
          onChange={e => setSum(parseFloat(e.target.value).toFixed(2))}
          disabled={isLoading}
        />
        <Button type="submit" disabled={isLoading}>
          Создать
        </Button>
      </form>
    </div>
  );
}

export default LobbyCreator;
