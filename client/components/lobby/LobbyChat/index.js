import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import Input from 'components/controllers/Input';
import Button from 'components/controllers/Button';
import socket from 'socket';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Innner lobby chat between players.
 * @param {{ className?: string }} props
 */
function LobbyChat({ className }) {
  // State Hooks
  const [messageList, setMessageList] = useState([]);
  const [message, setMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  // Selector Hooks
  const lobbyId = useSelector(state => state.lobby.lobby.id);

  // Document Event Handlers
  function handleMessageSubmit(e) {
    e.preventDefault();
    setErrorMessage('');
    if (message.length > 100) {
      return setErrorMessage('Сообщение не длинее 100 символов');
    }
    socket.emit('lobby:message', { lobbyId, message });
    setMessage('');
  }

  // Socket Events Handlers
  function handleMessaged(response) {
    setMessageList(oldMessageList => [...oldMessageList, response.data]);
  }

  function handleMessageError(response) {
    console.error(response);
  }

  // Effect Hooks
  useEffect(() => {
    socket.on('lobby:messaged', handleMessaged);
    socket.on('lobby:message:error', handleMessageError);
    return () => {
      socket.off('lobby:messaged', handleMessaged);
      socket.off('lobby:message:error', handleMessageError);
    };
  }, []);

  // Render
  return (
    <div className={classnames(styles.Container, className)}>
      <div className={styles.Body}>
        <div className={styles.MessageList}>
          {messageList.map((message, index) => (
            <div key={index} className={styles.Message}>
              <span className={styles.MessageAuthor}>{message.nickname}:</span>
              <span className={styles.MessageText}> {message.message}</span>
            </div>
          ))}
        </div>
        <div
          ref={div => {
            div?.scrollIntoView({ behavior: 'smooth' });
          }}
        ></div>
      </div>
      <div className={styles.Footer}>
        <form className={styles.SendMessageForm} onSubmit={handleMessageSubmit}>
          <Input
            name="message"
            label="Сообщение:"
            value={message}
            onChange={e => setMessage(e.target.value)}
            errorMessage={errorMessage}
          />
          <Button type="submit">Отправить</Button>
        </form>
      </div>
    </div>
  );
}

export default LobbyChat;
