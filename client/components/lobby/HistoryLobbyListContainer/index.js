import { useState, useEffect } from 'react';
import socket from 'socket';
import classnames from 'classnames';
import styles from './styles.module.css';

const HistoryLibbyListContainer = ({ className }) => {
  const [lobbyList, setLobbyList] = useState([]);
  
  // Socket Events Listeners
  const lobbyHistorySuccessListener = res => {
    setLobbyList(res.lobbyList);
  }
  
  const lobbyHistoryErrorListener = res => {
    console.error(res);
  }

  // Socket Events Subscription
  useEffect(() => {
    socket.on('lobby:history:success', lobbyHistorySuccessListener);
    socket.on('lobby:history:error', lobbyHistoryErrorListener);
    return () => {
      socket.off('lobby:history:success', lobbyHistorySuccessListener);
      socket.off('lobby:history:error', lobbyHistoryErrorListener);
    }
  });

  // Render
  return (
    <div className={classnames(styles.Container, className)}>
      <div className={styles.Header}>
        Список игр:
      </div>
      <div className={styles.Body}>
        <div className={styles.LobbyList}>
          {lobbyList.map(lobby => (
            <div className={styles.Lobby}>
              <div>{lobby.name}</div>
              <div>{lobby.status}</div>
              <div>{lobby.sum}</div>
              <div>{lobby?.owner?.nickname}</div>
              <div>{lobby?.opponent?.nickname}</div>
              <div>{lobby?.winner?.nickname}</div>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}


export default HistoryLibbyListContainer;
