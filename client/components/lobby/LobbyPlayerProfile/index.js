import { useSelector } from 'react-redux';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Displays player profile in the lobby.
 * @param {{
 *  playerKey: 'owner' | 'opponent';
 *  className?: string;
 * }} props 
 */
function LobbyPlayerProfile({ playerKey, className }) {
  // Selector Hooks
  const player = useSelector(state => state.lobby[playerKey]);

  // Render
  return (
    <div className={classnames(styles.Container, className)}>
      <img className={styles.Avatar} src={player.avatar} alt="Player" />
      <div className={styles.Nickname}>{player.nickname}</div>
    </div>
  );
}

export default LobbyPlayerProfile;
