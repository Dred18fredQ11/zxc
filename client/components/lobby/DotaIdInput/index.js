import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Input from 'components/controllers/Input';
import Button from 'components/controllers/Button';
import socket from 'socket';
import classnames from 'classnames';
import styles from './styles.module.css';

function DotaIdInput({ playerKey, className }) {
  const dispatch = useDispatch();

  // Selector Hooks
  const lobbyId = useSelector(state => state.lobby.lobby.id);
  const isUser = useSelector(state => {
    return state.auth.user.id === state.lobby[playerKey].id;
  });
  const owner = useSelector(state => state.lobby.owner);
  const isReady = useSelector(state => state.lobby[playerKey].isReady);

  // State Hooks
  const [isSet, setIsSet] = useState(false);
  const [dotaId, setDotaId] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  // Document Event Handlers
  function handleDotaIdSubmit(e) {
    e.preventDefault();
    if (!isUser) return;
    setErrorMessage('');
    if (dotaId.length > 16) {
      return setErrorMessage('Неверный Dota ID');
    }
    socket.emit('lobby:dotaId:set', { dotaId, lobbyId });
  }

  // Socket Event Handlers
  function handleDotaIdSetSuccess(response) {
    const localPlayerKey =
      response.data.userId === owner.id ? 'owner' : 'opponent';
    dispatch({
      type: 'lobby/setDotaId',
      payload: { playerKey: localPlayerKey, dotaId: response.data.dotaId },
    });
    if (localPlayerKey === playerKey) {
      setDotaId(response.data.dotaId ?? '');
      setIsSet(true);
    }
  }

  function handleDotaIdSetError(response) {
    console.error(response);
  }

  // Effect Hooks
  useEffect(() => {
    socket.on('lobby:dotaId:set:success', handleDotaIdSetSuccess);
    socket.on('lobby:dotaId:set:error', handleDotaIdSetError);
    return () => {
      socket.off('lobby:dotaId:set:success', handleDotaIdSetSuccess);
      socket.off('lobby:dotaId:set:error', handleDotaIdSetError);
    };
  }, []);

  // Render
  return (
    <form
      className={classnames(styles.Container, className)}
      onSubmit={handleDotaIdSubmit}
    >
      <Input
        className={styles.Input}
        variant="gray"
        borderVariant={isSet && 'green'}
        name="dota-id-field"
        label="Ваш id в Dota 2:"
        value={dotaId}
        onChange={e => {
          if (!isUser || isReady) return;
          setIsSet(false);
          setDotaId(e.target.value);
        }}
        errorMessage={errorMessage}
      />
      {!isSet && isUser && <Button className={styles.Submit}>Принять</Button>}
    </form>
  );
}

export default DotaIdInput;
