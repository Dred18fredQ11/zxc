import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSelector, useDispatch } from 'react-redux';
import LoaderPlaceholder from 'components/animations/LoaderPlaceholder';
import Button from 'components/controllers/Button';
import socket from 'socket';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * Wraps lobby list management logick.
 * @param {{ className?: string }} props
 */
function LobbyListContainer({ className }) {
  const router = useRouter();
  const dispatch = useDispatch();

  // State Hooks
  const [isLoading, setIsLoading] = useState(true);
  const [isJoining, setIsJoining] = useState(false);
  const [lobbyList, setLobbyList] = useState([]);

  // Selector Hooks
  const user = useSelector(state => state.auth.user);

  // Document Event Handlers
  function handleLobbyJoinClick(lobbyId) {
    return () => {
      if (isJoining) return;
      setIsJoining(true);
      dispatch({ type: 'lobby/joining/request' });
      socket.emit('lobby:join', { lobbyId });
    };
  }

  // Socket Event Handlers
  function handleLobbySuccess(response) {
    setLobbyList(response.data.lobbyList);
    setIsLoading(false);
  }

  function handleLobbyError(response) {
    console.error(response);
  }

  function handleLobbyCreated(response) {
    const lobby = { ...response.data.lobby, players: 1 };
    setLobbyList(oldLobbyList => [...oldLobbyList, lobby]);
  }

  function handleLobbyFilled(response) {
    setLobbyList(oldLobbyList => {
      return oldLobbyList.map(lobby => {
        if (lobby.id === response.data.lobbyId) return { ...lobby, players: 2 };
        return lobby;
      });
    });
  }

  function handleLobbyFreed(response) {
    setLobbyList(oldLobbyList => {
      return oldLobbyList.map(lobby => {
        if (lobby.id === response.data.lobbyId) return { ...lobby, players: 1 };
        return lobby;
      });
    });
  }

  function handleLobbyCanceled(response) {
    setLobbyList(oldLobbyList => {
      return oldLobbyList.filter(lobby => lobby.id !== response.data.lobbyId);
    });
  }

  function handleLobbyStarted(response) {
    setLobbyList(oldLobbyList => {
      return oldLobbyList.filter(lobby => lobby.id !== response.data.lobbyId);
    });
  }

  function handleLobbyJoinSuccess(response) {
    dispatch({
      type: 'lobby/joining/success',
      payload: { lobby: response.data.lobby, opponent: user },
    });
    router.push(`/lobby/${response.data.lobby.id}`);
  }

  function handleLobbyJoinError(response) {
    dispatch({ type: 'lobby/joining/error' });
    setIsJoining(false);
    console.error(response);
  }

  // Effect Hooks
  useEffect(() => {
    socket.on('lobby:list:success', handleLobbySuccess);
    socket.on('lobby:list:error', handleLobbyError);
    socket.on('lobby:created', handleLobbyCreated);
    socket.on('lobby:filled', handleLobbyFilled);
    socket.on('lobby:freed', handleLobbyFreed);
    socket.on('lobby:canceled', handleLobbyCanceled);
    socket.on('lobby:started', handleLobbyStarted);
    socket.on('lobby:join:success', handleLobbyJoinSuccess);
    socket.on('lobby:join:error', handleLobbyJoinError);
    socket.emit('lobby:list');
    return () => {
      socket.off('lobby:list:success', handleLobbySuccess);
      socket.off('lobby:list:error', handleLobbyError);
      socket.off('lobby:created', handleLobbyCreated);
      socket.off('lobby:filled', handleLobbyFilled);
      socket.off('lobby:freed', handleLobbyFreed);
      socket.off('lobby:canceled', handleLobbyCanceled);
      socket.off('lobby:started', handleLobbyStarted);
      socket.off('lobby:join:success', handleLobbyJoinSuccess);
      socket.off('lobby:join:error', handleLobbyJoinError);
    };
  }, []);

  // Render
  return (
    <div className={classnames(styles.Container, className)}>
      <div className={styles.Header}>
        <div className={styles.HeaderTitle}>
          <span className={styles.HeaderTitleText}>Поиск Лобби</span>
          <span className={styles.HeaderTitleCount}>
            (Доступно: {lobbyList.length})
          </span>
        </div>
      </div>
      <div className={styles.Body}>
        <div className={styles.Table}>
          <div className={styles.TableHeader}>
            <div className={styles.TableHeaderName}>Имя</div>
            <div className={styles.TableHeaderSum}>Сумма</div>
            <div className={styles.TableHeaderOwner}>Хост</div>
          </div>
          <div className={styles.TableBody}>
            {isLoading ? (
              <LoaderPlaceholder />
            ) : (
              <div className={styles.TableBodyWrapper}>
                {lobbyList.map(lobby => (
                  <div key={lobby.id} className={styles.TableRow}>
                    <div className={styles.TableRowName}>{lobby.name}</div>
                    <div className={styles.TableRowSum}>{lobby.sum}</div>
                    <div className={styles.TableRowOwner}>
                      {lobby.ownerNickname}
                    </div>
                    <div className={styles.TableRowPlayers}>
                      {lobby.players}/2
                    </div>
                    {!lobby.opponentId && (
                      <Button
                        className={styles.TableRowButton}
                        type="button"
                        onClick={handleLobbyJoinClick(lobby.id)}
                        disabled={isJoining}
                      >
                        Войти
                      </Button>
                    )}
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default LobbyListContainer;
