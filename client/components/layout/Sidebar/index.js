import { useState } from 'react';
import { useRouter } from 'next/router';
import PageLobbiesIcon from 'components/icons/PageLobbiesIcon';
import PageProfileIcon from 'components/icons/PageProfileIcon';
import PageHelpIcon from 'components/icons/PageHelpIcon';
import Link from 'next/link';
import classnames from 'classnames';
import styles from './styles.module.css';

function SidebarLink({ Icon, text, href, className, isOutside = false }) {
  const [isHover, setIsHover] = useState(false);
  const isActive = useRouter().asPath === href;
  if (isOutside) {
    return (
      <a
        className={classnames(styles.link, className)}
        href={href}
        target="__blank"
        onMouseEnter={() => setIsHover(true)}
        onMouseLeave={() => setIsHover(false)}
      >
        <Icon
          className={styles.linkIcon}
          fill={isActive || isHover ? '#F2F2F7' : '#8E8E93'}
        />
        <span
          className={classnames(
            styles.linkText,
            (isActive || isHover) && styles.linkTextActive,
          )}
        >
          {text}
        </span>
      </a>
    );
  }

  return (
    <Link href={href} passHref>
      <a
        className={classnames(styles.link, className)}
        onMouseEnter={() => setIsHover(true)}
        onMouseLeave={() => setIsHover(false)}
      >
        <Icon
          className={styles.linkIcon}
          fill={isActive || isHover ? '#F2F2F7' : '#8E8E93'}
        />
        <span
          className={classnames(
            styles.linkText,
            (isActive || isHover) && styles.linkTextActive,
          )}
        >
          {text}
        </span>
      </a>
    </Link>
  );
}

export default function Sidebar({ className }) {
  return (
    <aside className={classnames(styles.wrapper, className)}>
      <nav className={styles.nav}>
        <SidebarLink
          href="/lobby"
          Icon={PageLobbiesIcon}
          text="Играть"
        />
        <SidebarLink
          href="/profile"
          Icon={PageProfileIcon}
          text="Мой профиль"
        />
        <SidebarLink
          href="https://discord.com/invite/KyjunKMWJ9"
          Icon={PageHelpIcon}
          text="Помощь"
          isOutside
        />
      </nav>
    </aside>
  );
}
