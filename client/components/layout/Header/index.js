import Link from 'next/link';
import Image from 'next/image';
import { useSelector } from 'react-redux';
import Button from 'components/controllers/Button';
import Dropdown from 'components/popups/Dropdown';
import ProfileBoard from 'components/user/ProfileBoard';
import BalanceBoard from 'components/user/BalanceBoard';
import classnames from 'classnames';
import styles from './styles.module.css';

export default function Header({ className }) {
  const isAuth = useSelector(state => {
    return state.auth.fetchingUserStatus === 'succeeded';
  });
  return (
    <header className={classnames(styles.header, className)}>
      <Link href="/" passHref>
        <a className={styles.brand}>
          <Image src="/assets/brand.svg" width={50} height={48} alt="Brand" />
          <span className={styles.brandText}>Go_ZXC</span>
        </a>
      </Link>

      <div className={styles.rightPanel}>
        {!isAuth && (
          <a className={styles.signIn} href="/api/auth/twitch">
            <span>Войти</span>
          </a>
        )}
        {isAuth && (
          <>
            <BalanceBoard />
            <ProfileBoard />
            <Dropdown Toggler={Burger} Dropdown={Navigation} />
          </>
        )}
      </div>
    </header>
  );
}

function Burger({ onClick }) {
  return (
    <button className={classnames(styles.toggle)} onClick={onClick}>
      <span></span>
      <span></span>
      <span></span>
    </button>
  );
}

function Navigation({ className }) {
  return (
    <nav className={classnames(styles.nav, className)}>
      <Link href="/lobby">
        <a className={styles.navLink}>Список Лобби</a>
      </Link>
    </nav>
  );
}
