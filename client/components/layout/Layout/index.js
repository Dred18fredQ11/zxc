import Head from 'next/head';
import Header from 'components/layout/Header';
import Sidebar from 'components/layout/Sidebar';
import Footer from 'components/layout/Footer';
import classnames from 'classnames';
import styles from './styles.module.css';

/**
 * @param {{
 *  children?: JSX.Element;
 *  className?: string;
 *  title?: string;
 *  header?: 'normal' | 'none';
 *  footer?: 'normal' | 'none';
 *  sidebar?: 'normal' | 'none';
 * }} props
 */
export default function Layout({
  children,
  className,
  title,
  header = 'normal',
  footer = 'normal',
  sidebar = 'normal',
}) {
  return (
    <>
      <Head>
        <title>{title ?? 'Go_ZXC'}</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap"
          rel="stylesheet"
        />
      </Head>

      {header === 'normal' && <Header />}
      {sidebar === 'normal' && <Sidebar />}

      <main
        className={classnames(
          styles.main,
          sidebar === 'normal' && styles.mainWithSidebar,
          className,
        )}
      >
        {children}
      </main>

      {footer === 'normal' && <Footer />}
    </>
  );
}
