import { useState } from 'react';
import Image from 'next/image';
import TailspinLoader from 'components/animations/TailspinLoader';
import classnames from 'classnames';
import styles from './styles.module.css';

export default function Input({
  variant = 'default',
  borderVariant = 'default',
  label,
  id,
  name,
  value,
  onChange,
  errorMessage = '',
  disabled = false,
  type = 'text',
  className,
  min,
  max,
}) {
  const [isFocus, setIsFocus] = useState(false);
  const handleFocus = () => setIsFocus(true);
  const handleBlur = () => setIsFocus(false);
  const fieldsetClass = classnames(
    styles.fieldset,
    (variant === 'gray') && styles.gray,
    (borderVariant === 'green') && styles.greenBorder,
    (isFocus || value) && styles.fieldsetFocused,
    (errorMessage && !disabled) && styles.fieldsetError,
    disabled && styles.fieldsetDisabled,
    className,
  );
  return (
    <fieldset className={fieldsetClass}>
      <label htmlFor={id} className={styles.label}>
        {label}
      </label>
      <input
        id={id}
        name={name}
        className={styles.input}
        type={type}
        value={value}
        onChange={onChange}
        onFocus={handleFocus}
        onBlur={handleBlur}
        disabled={disabled}
        min={min}
        max={max}
      />
      {disabled && <TailspinLoader className={styles.loader} />}
      {(errorMessage && !disabled) && (
        <>
          <span className={styles.errorIcon}>
            <Image
              src="/assets/inputErrorIcon.svg"
              width={25}
              height={25}
              alt="Input Error"
            />
          </span>
          <span className={styles.errorMessage}>{errorMessage}</span>
        </>
      )}
    </fieldset>
  );
}
