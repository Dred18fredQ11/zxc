import classnames from 'classnames';
import styles from './styles.module.css';

export default function Button({
  children,
  className,
  onClick,
  disabled = false,
  isActive = false,
  type = '',
  variant = 'gray',
}) {
  return (
    <button
      className={classnames(
        styles.button,
        variant === 'green' && styles.green,
        isActive && styles.active,
        className,
      )}
      type={type}
      onClick={onClick}
      disabled={disabled}
    >
      <span>{children}</span>
    </button>
  );
}
