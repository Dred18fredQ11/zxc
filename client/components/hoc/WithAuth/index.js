import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import Layout from 'components/layout/Layout';
import LoaderPlaceholder from 'components/animations/LoaderPlaceholder';

export default function WithAuth(Component) {
  function Auth(props) {
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(true);
    const isAuth = useSelector(state => {
      return state.auth.fetchingUserStatus === 'succeeded';
    });
    useEffect(() => {
      if (!localStorage.getItem('jwt')) return router.push('/');
      if (isAuth) setIsLoading(false)
    }, [isAuth]);
    if (isLoading) {
      return (
        <Layout
          title="ZXC - Loading..."
          header="none"
          footer="none"
          sidebar="none"
        >
          <LoaderPlaceholder />
        </Layout>
      );
    }

    return <Component {...props} />;
  }

  return Auth;
}
