export function appendHeaderJWT(options = {}) {
  const token = localStorage.getItem('jwt');
  return {
    ...options,
    headers: {
      ...(options.headers ?? {}),
      Authorization: `Bearer ${token}`,
    },
  };
}
