import Link from 'next/link';
import Layout from 'components/layout/Layout';
import styles from 'styles/AuthError.module.css';

function AuthErrorPage() {
  return (
    <Layout
      title="Go_ZXC - Ошибка авторизации"
      sidebar="none"
      header="none"
      footer="none"
      banners="none"
      className={styles.Main}
    >
      <h1 className={styles.Title}>Ошибка авторизации</h1>
      <Link href="/" passHref>
        <a className={styles.Link}>На главную</a>
      </Link>
    </Layout>
  );
}

export default AuthErrorPage;
