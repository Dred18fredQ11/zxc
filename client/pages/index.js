import Image from 'next/image';
import Layout from 'components/layout/Layout';
import { Container, Row, Col } from 'react-bootstrap';
import styles from 'styles/Home.module.css';

export default function Home() {
  return (
    <Layout title="Go_ZXC - Главная" className={styles.main} sidebar="none">
      <section className={styles.banner}>
        <Container className={styles.container}>
          <h1 className={styles.bannerTitle}>Welcome to the ZXC Project</h1>
          <h2 className={styles.bannerSubtitle}>
            Начните использовать прямо сейчас!
          </h2>
          <a href="/api/auth/twitch" className={styles.bannerAuthorization}>
            <span>Авторизироваться</span>
          </a>
          <button className={styles.bannerScroll}>
            <span className={styles.bannerScrollImg}>
              <Image src="/assets/arrow-down.svg" width={30} height={30} />
            </span>
            <span className={styles.bannerScrollText}>Scroll</span>
          </button>
        </Container>
      </section>
    </Layout>
  );
}
