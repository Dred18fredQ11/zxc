import Layout from 'components/layout/Layout';
import WithAuth from 'components/hoc/WithAuth';
import HistoryLobbyListContainer from 'components/lobby/HistoryLobbyListContainer';

function ProfilePage() {
  return (
    <Layout title="Go_ZXC">
      <HistoryLobbyListContainer />
    </Layout>
  );
}

export default WithAuth(ProfilePage);
