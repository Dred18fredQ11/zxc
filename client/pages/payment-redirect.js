import Link from 'next/link';
import Layout from 'components/layout/Layout';
import styles from 'styles/PaymentRedirect.module.css';

function PaymentRedirectPage() {
  return (
    <Layout
      title="Go_ZXC - Успешный платеж"
      sidebar="none"
      header="none"
      footer="none"
      banners="none"
      className={styles.Main}
    >
      <h1 className={styles.Title}>Ваш платеж прошел успешно</h1>
      <Link href="/" passHref>
        <a className={styles.Link}>На главную</a>
      </Link>
    </Layout>
  );
}

export default PaymentRedirectPage;
