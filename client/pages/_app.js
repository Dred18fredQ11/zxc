import { Provider } from 'react-redux';
import { useStore } from '../store';
import { NotificationContainer } from 'react-notifications';
import Authorizer from 'components/wrappers/Authorizer';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'styles/variables.css';
import 'styles/globals.css';

function App({ Component, pageProps }) {
  const store = useStore();
  return (
    <Provider store={store}>
      <Authorizer>
        <Component {...pageProps} />
        <NotificationContainer />
      </Authorizer>
    </Provider>
  );
}

export default App;
