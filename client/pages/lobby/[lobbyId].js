import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSelector, useDispatch } from 'react-redux';
import Layout from 'components/layout/Layout';
import LoaderPlaceholder from 'components/animations/LoaderPlaceholder';
import LobbyPlayerProfile from 'components/lobby/LobbyPlayerProfile';
import LobbyChat from 'components/lobby/LobbyChat';
import ItemListContainer from 'components/lobby/ItemListContainer';
import LobbyReadyButton from 'components/lobby/LobbyReadyButton';
import DotaIdInput from 'components/lobby/DotaIdInput';
import { NotificationManager } from 'react-notifications';
import socket from 'socket';
import styles from 'styles/LobbyPage.module.css';

function LobbyRoomPage() {
  const router = useRouter();
  const dispatch = useDispatch();

  // Selector Hooks
  const fetchingStatus = useSelector(state => state.lobby.fetchingLobbyStatus);
  const lobby = useSelector(state => state.lobby.lobby);
  const owner = useSelector(state => state.lobby.owner);
  const opponent = useSelector(state => state.lobby.opponent);

  // Socket Event Handlers
  function handleLobbyJoined(response) {
    dispatch({
      type: 'lobby/joined',
      payload: { opponent: response.data.user },
    });
  }

  function handleLobbyLeaved(response) {
    if (response.data.userId === owner.id) {
      dispatch({
        type: 'lobby/canceled',
      });
      router.push('/lobby');
    } else {
      dispatch({
        type: 'lobby/leaved',
      });
    }
  }

  function handleLobbyStarted(response) {
    if (response.data.lobbyId === lobby.id) {
      dispatch({ type: 'lobby/started' });
      NotificationManager.success(
        'Игра Началась. Вы будете приглашены в лобби',
      );
    }
  }

  // Effect Hooks
  useEffect(() => {
    if (fetchingStatus !== 'success') return router.push('/lobby');

    function routeChange(url) {
      if (router.asPath === url) return;
      socket.emit('lobby:leave', { lobbyId: lobby.id });
      dispatch({ type: 'lobby:leave' });
    }

    router.events.on('routeChangeStart', routeChange);
    socket.on('lobby:joined', handleLobbyJoined);
    socket.on('lobby:leaved', handleLobbyLeaved);
    socket.on('lobby:started', handleLobbyStarted);
    return () => {
      router.events.off('routeChangeStart', routeChange);
      socket.off('lobby:joined', handleLobbyJoined);
      socket.off('lobby:leaved', handleLobbyLeaved);
      socket.off('lobby:started', handleLobbyStarted);
    };
  }, []);

  useEffect(() => {
    if (fetchingStatus === 'error') router.push('/lobby');
  }, [fetchingStatus]);

  // Render
  if (['idle', 'loading', 'error'].includes(fetchingStatus)) {
    return (
      <Layout title="Go_ZXC - Loading..." sidebar="none">
        <LoaderPlaceholder />
      </Layout>
    );
  }

  return (
    <Layout title="Go_ZXC - Лобби" className={styles.Main}>
      <div className={styles.OwnerBlock}>
        <LobbyPlayerProfile
          className={styles.LobbyPlayerProfile}
          playerKey="owner"
        />
        <DotaIdInput className={styles.DotaInputId} playerKey="owner" />
        <ItemListContainer />
        <LobbyReadyButton playerKey="owner" />
      </div>
      {opponent && (
        <div className={styles.OpponentBlock}>
          <LobbyPlayerProfile
            className={styles.LobbyPlayerProfile}
            playerKey="opponent"
          />
          <DotaIdInput className={styles.DotaInputId} playerKey="opponent" />
          <LobbyReadyButton playerKey="opponent" />
        </div>
      )}
      <LobbyChat className={styles.LobbyChat} />
    </Layout>
  );
}

export default LobbyRoomPage;

// import { useState, useEffect } from 'react';
// import { useRouter } from 'next/router';
// import { useSelector } from 'react-redux';
// import Layout from 'components/layout/Layout';
// import LoaderPlaceholder from 'components/animations/LoaderPlaceholder';
// import LobbyChat from 'components/lobby/LobbyChat';
// import PlayerManager from 'components/lobby/PlayerManager';
// import socket from 'socket';
// import styles from 'styles/Lobby.module.css';

// export default function LobbyGame() {
//   const lobbyId = useRouter().query.lobbyId;

//   // State Hooks
//   const [isLoading, setIsLoading] = useState(true);
//   const [lobby, setLobby] = useState();
//   const [playerLeft, setPlayerLeft] = useState();
//   const [playerRight, setPlayerRight] = useState();
//   const [isOwner, setIsOwner] = useState(false);

//   // Selector Hooks
//   const userId = useSelector(state => state?.auth?.user?.id);

//   // Event Handlers
//   function handleJoinRoomSuccess({ data: { lobby, owner, opponent } }) {
//     setLobby(lobby);
//     if (owner.id === userId) {
//       setPlayerLeft(owner);
//       setPlayerRight(opponent);
//       setIsOwner(true);
//     } else if (opponent.id === userId) {
//       setPlayerLeft(opponent);
//       setPlayerRight(owner);
//       setIsOwner(false);
//     }
//     setIsLoading(false);
//   }

//   function handleJoinRoomError(response) {
//     console.log(response);
//   }

//   function handleJoinSuccess({ data: { lobby, user} }) {
//     if (user.id === userId) {
//       setPlayerLeft(user);
//     } else {
//       setPlayerRight(user);
//     }
//   }

//   function handleJoinError(response) {
//     console.log(response);
//   }

//   useEffect(() => {
//     socket.on('lobby:join-room-success', handleJoinRoomSuccess);
//     socket.on('lobby:join-room-error', handleJoinRoomError);
//     socket.on('lobby:join-success', handleJoinSuccess);
//     socket.on('lobby:join-error', handleJoinError)
//     socket.emit('lobby:join-room', { lobbyId });
//     return () => {
//       socket.off('lobby:join-room-success', handleJoinRoomSuccess);
//       socket.off('lobby:join-room-error', handleJoinRoomError);
//       socket.off('lobby:join-success', handleJoinSuccess);
//       socket.off('lobby:join-error', handleJoinError);
//     };
//   }, []);

//   if (isLoading) {
//     return (

//     );
//   }

//   return (
//     <Layout title="Go_ZXC - Лобби" className={styles.mainLobby}>
//       <div className={styles.playerManagerWrapper}>
//         <PlayerManager lobby={lobby} player={playerLeft} isOwner={isOwner} />
//         {playerRight && (
//           <PlayerManager
//             lobby={lobby}
//             player={playerRight}
//             isOwner={!isOwner}
//           />
//         )}
//       </div>
//       <LobbyChat className={styles.lobbyChat} />
//     </Layout>
//   );
// }
