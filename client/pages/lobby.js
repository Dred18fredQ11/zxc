import Layout from 'components/layout/Layout';
import LobbyListContainer from 'components/lobby/LobbyListContainer';
import LobbyCreator from 'components/lobby/LobbyCreator';
import WithAuth from 'components/hoc/WithAuth';
import styles from 'styles/Lobby.module.css';

function Lobby() {
  return (
    <Layout title="Go_ZXC - Список Лобби" className={styles.Main}>
      <LobbyListContainer />
      <LobbyCreator />
    </Layout>
  );
}

export default WithAuth(Lobby);
