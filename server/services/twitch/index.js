const axios = require('axios');
const { clientId, redirectUri, clientSecret } = require('../../config').twitch;

function formAuthUrl() {
  const url = new URL('https://id.twitch.tv/oauth2/authorize');
  url.searchParams.append('client_id', clientId);
  url.searchParams.append('redirect_uri', redirectUri);
  url.searchParams.append('response_type', 'code');
  url.searchParams.append('scope', 'user_read');
  return url.href;
}

function formRequestTokenUrl(code) {
  const url = new URL('https://id.twitch.tv/oauth2/token');
  url.searchParams.append('client_id', clientId);
  url.searchParams.append('client_secret', clientSecret);
  url.searchParams.append('redirect_uri', redirectUri);
  url.searchParams.append('code', code);
  url.searchParams.append('grant_type', 'authorization_code');
  return url.href;
}

async function requestToken(code) {
  const url = formRequestTokenUrl(code);
  const response = await axios.post(url);
  return response.data;
}

async function requestUserData(token) {
  const url = 'https://api.twitch.tv/helix/users';
  const options = {
    headers: {
      'Client-ID': clientId,
      Authorization: `Bearer ${token}`,
    },
  };
  const response = await axios.get(url, options);
  return response.data.data[0];
}

const twitchAuthUrl = formAuthUrl();

module.exports = {
  twitchAuthUrl,
  requestToken,
  requestUserData,
}
