const { QueryTypes } = require('sequelize');
const { sequelize } = require('../models');

exports.isExists = async function isExists(id) {
  const query = await sequelize.query(
    'SELECT EXISTS(SELECT id FROM `User` WHERE id=?) AS isExists',
    {
      replacements: [id],
      type: QueryTypes.SELECT,
      plain: true,
      raw: true,
    },
  );
  return Boolean(query?.isExists);
};
