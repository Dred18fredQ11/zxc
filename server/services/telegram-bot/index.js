const axios = require('axios');
const qs = require('qs');
const { chatId, sendMessageUrl } = require('../../config').telegramBot;

async function sendMessage(text) {
  const response = await axios({
    method: 'post',
    url: sendMessageUrl,
    data: qs.stringify({
      chat_id: chatId,
      text,
    }),
    headers: {
      'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
    }
  });
  console.log('GAME_STARTED', response.data);
  return true;
}

module.exports = {
  sendMessage,
};
