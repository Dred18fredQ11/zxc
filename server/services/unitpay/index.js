const { SHA256 } = require('crypto-js');
const { secretKey, projectId, redirectUrl } = require('../../config').unitpay;

class UnitpayApiRequestError extends Error {}

function formSignature(account, currency, desc, sum) {
  return SHA256([account, currency, desc, sum, secretKey].join('{up}'));
}

function formPaymentInitUrl(account, sum, currency, desc, clientIp) {
  const signature = formSignature(account, currency, desc, sum);
  const url = new URL('https://unitpay.money/api');
  url.searchParams.append('method', 'initPayment');
  url.searchParams.append('params[paymentType]', 'card');
  url.searchParams.append('params[account]', account);
  url.searchParams.append('params[sum]', sum);
  url.searchParams.append('params[currency]', currency);
  url.searchParams.append('params[projectId]', projectId);
  url.searchParams.append('params[resultUrl]', redirectUrl);
  url.searchParams.append('params[desc]', desc);
  url.searchParams.append('params[ip]', clientIp);
  url.searchParams.append('params[secretKey]', secretKey);
  url.searchParams.append('params[signature]', signature);
  return url.href;
}

function requestPaymentInit(account, sum, currency, desc, clientIp) {
  const url = formPaymentInitUrl(account, sum, currency, desc, clientIp);
  const response = await axios.get(url);
  if (unitpayResponse?.data?.error || !unitpayResponse?.data?.result) {
    const error = response?.data?.error
    const message = `code: ${error?.code} - ${error?.message}`
    throw new UnitpayApiRequestError(`Ошибка платежка: ${message}`)
  }
  return unitpayResponse?.data?.result;
}

module.exports = { requestPaymentInit };
