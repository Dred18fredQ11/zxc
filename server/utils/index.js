const { Socket } = require('socket.io');

/**
 * Wrapper for socket success response.
 * @param {object} data
 */
function success(data) {
  return { message: 'ok', data }
}

/**
 * Wrapper for socket error response.
 * @param {string} description
 */
function error(description) {
  return { message: 'error', data: { description }}
}

/**
 * Decorator for handlind socket exceptions.
 * @param {Function} func
 * @param {Socket} socket
 * @param {string} errorEvent
 */
function catchError(func, socket, errorEvent) {
  return async function (...args) {
    try {
      return await func(...args)
    } catch (e) {
      console.log(e);
      if (socket && errorEvent) {
        socket.emit(errorEvent, error(e.message))
      }
    }
  }
}

module.exports = {
  success,
  error,
  catchError,
}
