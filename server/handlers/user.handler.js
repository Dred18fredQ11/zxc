const { User } = require('../models');
const { success, catchError } = require('../utils');
const { NotAuthorizedError, UserNotFoundError } = require('../errors');

module.exports = (socket, io) => {
  /**
   * Returns requested user data.
   */
  socket.on(
    'user:data',
    catchError(
      async () => {
        const userId = socket?.decoded?.id;
        if (!userId) throw new NotAuthorizedError();

        const user = await User.findByPk(userId, { raw: true });
        if (!user) throw new UserNotFoundError();

        socket.emit('user:data:success', success({ user }));
      },
      socket,
      'user:data:error',
    ),
  );
};
