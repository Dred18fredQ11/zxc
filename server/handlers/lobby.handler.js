const { Op, Sequelize } = require('sequelize');
const { Lobby, User } = require('../models');
const { sendMessage } = require('../services/telegram-bot');
const { success, catchError } = require('../utils');
const {
  NotAuthorizedError,
  LobbyIsFullError,
  LobbyNotFoundError,
  UserNotFoundError,
} = require('../errors');

module.exports = (socket, io) => {
  /**
   * Returns list of hosted lobbies to requested user.
   */
  socket.on(
    'lobby:list',
    catchError(
      async () => {
        const userId = socket?.decoded?.id;
        if (!userId) throw new NotAuthorizedError();

        const lobbyList = await Lobby.findAll({
          attributes: [
            'id',
            'name',
            'sum',
            [
              Sequelize.literal(
                '((ownerId is not null) + (opponentId is not null))',
              ),
              'players',
            ],
            [Sequelize.col('owner.nickname'), 'ownerNickname'],
          ],
          where: { status: 'waiting' },
          include: {
            model: User,
            as: 'owner',
            attributes: [],
          },
          raw: true,
        });

        socket.emit('lobby:list:success', success({ lobbyList }));
      },
      socket,
      'lobby:list:error',
    ),
  );

  /**
   * Returns history of the games.
   */
  socket.on(
    'lobby:history',
    catchError(async () => {
      const userId = socket?.decoded?.id;
      if (!userId) throw new NotAuthorizedError();

      const lobbyList = await Lobby.findAll({
        attributes: ['id', 'createdAt', 'status', 'sum'],
        where: { ownerId },
        include: [
          {
            model: User,
            as: 'owner',
            attributes: ['id', 'nickname'],
          },
          {
            model: User,
            as: 'opponent',
            attributes: ['id', 'nickname'],
          },
          {
            model: User,
            as: 'winner',
            attributes: ['id', 'nickname'],
          },
        ],
        raw: true,
      });
      socket.emit('lobby:history:success', { lobbyList });
    }, socket, 'lobby:history:error'),
  );

  /**
   * Returns lobby data to requested user.
   */
  socket.on(
    'lobby:lobby',
    catchError(async ({ lobbyId }) => {
      const userId = socket?.decoded?.id;
      if (!userId) throw new NotAuthorizedError();

      const lobby = await Lobby.findByPk(lobbyId, {
        attributes: [
          'id',
          'name',
          'sum',
          'status',
          'ownerDotaId',
          'opponentDotaId',
          'isOwnerReady',
          'isOpponentReady',
          'isMangoBanned',
          'isCloakBanned',
          'isHatBanned',
          'isRaindropsBanned',
          'isStickBanned',
          'isBottleBanned',
        ],
        include: [
          {
            model: User,
            as: 'owner',
            attributes: ['id', 'nickname', 'avatar'],
          },
          {
            model: User,
            as: 'opponent',
            attributes: ['id', 'nickname', 'avatar'],
          },
        ],
        raw: true,
      });
      if (!lobby) throw new LobbyNotFoundError();

      socket.join(`lobby:${lobbyId}`);
      io.to(`lobby:${lobbyId}`).emit('lobby:lobby:success', success(lobby));
    }),
    socket,
    'lobby:lobby:error',
  );

  /**
   * Creates lobby and returns its data.
   */
  socket.on(
    'lobby:create',
    catchError(async ({ name, sum }) => {
      const userId = socket?.decoded?.id;
      if (!userId) throw new NotAuthorizedError();

      if (typeof name !== 'string' || typeof sum !== 'number' || isNaN(sum)) {
        throw new TypeError('Wrong data type');
      }

      const user = await User.findByPk(userId, {
        attributes: ['nickname'],
        raw: true,
      });

      const lobby = await Lobby.create({
        name,
        sum,
        ownerId: userId,
      });

      const creatorResponse = {
        id: lobby.id,
        name: lobby.name,
        sum: lobby.sum,
        status: lobby.status,
        isMangoBanned: lobby.isMangoBanned,
        isCloakBanned: lobby.isCloakBanned,
        isHatBanned: lobby.isHatBanned,
        isRaindropsBanned: lobby.isRaindropsBanned,
        isStickBanned: lobby.isStickBanned,
        isBottleBanned: lobby.isBottleBanned,
      };
      const everyoneResponse = {
        id: lobby.id,
        name: lobby.name,
        sub: lobby.sum,
        ownerId: userId,
        opponentId: null,
        ownerNickname: user.nickname,
      };

      socket.join(`lobby:${lobby.id}`);
      socket.emit('lobby:create:success', success({ lobby: creatorResponse }));
      io.emit('lobby:created', success({ lobby: everyoneResponse }));
    }),
    socket,
    'lobby:create:error',
  );

  /**
   * Connects user to hosted lobby.
   */
  socket.on(
    'lobby:join',
    catchError(
      async ({ lobbyId }) => {
        const userId = socket?.decoded?.id;
        if (!userId) throw new NotAuthorizedError();

        const user = await User.findByPk(userId, {
          attributes: ['id', 'nickname', 'avatar'],
          raw: true,
        });
        if (!user) throw new UserNotFoundError();

        const lobby = await Lobby.findByPk(lobbyId, {
          attributes: [
            'id',
            'name',
            'sum',
            'status',
            'ownerDotaId',
            'isOwnerReady',
            'isOpponentReady',
            'isMangoBanned',
            'isCloakBanned',
            'isHatBanned',
            'isRaindropsBanned',
            'isStickBanned',
            'isBottleBanned',
            'opponentId',
          ],
          include: [
            {
              model: User,
              as: 'owner',
              attributes: ['id', 'nickname', 'avatar'],
            },
          ],
        });
        if (!lobby) throw new LobbyNotFoundError();

        if (lobby.opponentId) throw new LobbyIsFullError();
        if (lobby.owner.id === userId) {
          throw new Error('Bug, cannot enter to himself');
        }

        lobby.opponentId = userId;
        await lobby.save();

        io.to(`lobby:${lobby.id}`).emit('lobby:joined', success({ user }));
        socket.join(`lobby:${lobby.id}`);
        socket.emit('lobby:join:success', success({ lobby: lobby.toJSON() }));
        io.emit('lobby:filled', success({ lobbyId }));
      },
      socket,
      'lobby:join:error',
    ),
  );

  /**
   * Inside lobby messages between users.
   */
  socket.on(
    'lobby:message',
    catchError(
      async ({ lobbyId, message }) => {
        const userId = socket?.decoded?.id;
        if (!userId) throw new NotAuthorizedError();

        const user = await User.findByPk(userId, {
          attributes: ['nickname'],
          raw: true,
        });
        if (!user) throw new UserNotFoundError();

        io.to(`lobby:${lobbyId}`).emit(
          'lobby:messaged',
          success({ nickname: user.nickname, message }),
        );
      },
      socket,
      'lobby:message:error',
    ),
  );

  /**
   * Changes item status to banned and returns banned item name to room.
   */
  socket.on(
    'lobby:item:ban',
    catchError(
      async ({ lobbyId, itemName }) => {
        const userId = socket?.decoded?.id;
        if (!userId) throw new NotAuthorizedError();

        const lobby = await Lobby.findByPk(lobbyId, {
          attributes: ['id', 'ownerId'],
        });
        if (!lobby) throw new LobbyNotFoundError();
        if (lobby.ownerId !== userId) throw new Error('Not owner');

        const itemStatusDict = {
          mango: 'isMangoBanned',
          cloak: 'isCloakBanned',
          hat: 'isHatBanned',
          raindrops: 'isRaindropsBanned',
          stick: 'isStickBanned',
          bottle: 'isBottleBanned',
        };
        if (!itemStatusDict[itemName])
          throw new Error('There are no such item');

        lobby[itemStatusDict[itemName]] = true;
        await lobby.save();

        io.to(`lobby:${lobby.id}`).emit(
          'lobby:item:banned',
          success({ itemName }),
        );
      },
      socket,
      'socket:item:ban:error',
    ),
  );

  /**
   * Changes item status to unbanned and returns banned item name to room.
   */
  socket.on(
    'lobby:item:unban',
    catchError(
      async ({ lobbyId, itemName }) => {
        const userId = socket?.decoded?.id;
        if (!userId) throw new NotAuthorizedError();

        const lobby = await Lobby.findByPk(lobbyId, {
          attributes: ['id', 'ownerId'],
        });
        if (!lobby) throw new LobbyNotFoundError();
        if (lobby.ownerId !== userId) throw new Error('Not owner');

        const itemStatusDict = {
          mango: 'isMangoBanned',
          cloak: 'isCloakBanned',
          hat: 'isHatBanned',
          raindrops: 'isRaindropsBanned',
          stick: 'isStickBanned',
          bottle: 'isBottleBanned',
        };
        if (!itemStatusDict[itemName])
          throw new Error('There are no such item');

        lobby[itemStatusDict[itemName]] = false;
        await lobby.save();

        io.to(`lobby:${lobby.id}`).emit(
          'lobby:item:unbanned',
          success({ itemName }),
        );
      },
      socket,
      'socket:item:ban:error',
    ),
  );

  /**
   * Changes player status to ready.
   */
  socket.on(
    'lobby:ready',
    catchError(
      async ({ lobbyId }) => {
        const userId = socket?.decoded?.id;
        if (!userId) throw new NotAuthorizedError();

        const lobby = await Lobby.findByPk(lobbyId, {
          attributes: [
            'id',
            'ownerId',
            'opponentId',
            'isOwnerReady',
            'isOpponentReady',
            'ownerDotaId',
            'opponentDotaId',
            'isMangoBanned',
            'isCloakBanned',
            'isHatBanned',
            'isRaindropsBanned',
            'isStickBanned',
            'isBottleBanned',
          ],
        });
        if (!lobby) throw new LobbyNotFoundError();

        if (lobby.ownerId === userId) {
          if (!lobby.ownerDotaId) throw new Error('Dota ID is required');
          lobby.isOwnerReady = true;
        } else if (lobby.opponentId === userId) {
          if (!lobby.opponentDotaId) throw new Error('Dota ID is required');
          lobby.isOpponentReady = true;
        } else {
          throw new Error('User not in lobby');
        }
        await lobby.save();

        if (lobby.isOwnerReady && lobby.isOpponentReady) {
          lobby.status = 'playing';
          await lobby.save();

          const message =
            'Game started' +
            `\nLobby ID: ${lobby.id}` +
            `\nSum: ${lobby.sum}` +
            `\nOwner Dota ID: ${lobby.ownerDotaId}.` +
            `\nOpponent Dota ID: ${lobby.opponentDotaId}.` +
            `\nMango: ${!lobby.isMangoBanned ? 'yes' : 'no'}` +
            `\nCloak: ${!lobby.isCloakBanned ? 'yes' : 'no'}` +
            `\nHat: ${!lobby.isHatBanned ? 'yes' : 'no'}` +
            `\nRaindrops: ${!lobby.isRaindropsBanned ? 'yes' : 'no'}` +
            `\nStick: ${!lobby.isStickBanned ? 'yes' : 'no'}` +
            `\nBottle: ${!lobby.isBottleBanned ? 'yes' : 'no'}`;
          await sendMessage(message);
          io.emit('lobby:started', success({ lobbyId: lobby.id }));
        }
        io.to(`lobby:${lobby.id}`).emit(
          'lobby:ready:success',
          success({ userId }),
        );
      },
      socket,
      'lobby:ready:error',
    ),
  );

  /**
   * Changes player status to unready.
   */
  socket.on(
    'lobby:unready',
    catchError(
      async ({ lobbyId }) => {
        const userId = socket?.decoded?.id;
        if (!userId) throw new NotAuthorizedError();

        const lobby = await Lobby.findByPk(lobbyId, {
          attributes: [
            'id',
            'ownerId',
            'opponentId',
            'isOwnerReady',
            'isOpponentReady',
            'ownerDotaId',
            'opponentDotaId',
          ],
        });
        if (!lobby) throw new LobbyNotFoundError();

        if (lobby.ownerId === userId) {
          lobby.isOwnerReady = false;
        } else if (lobby.opponentId === userId) {
          lobby.isOpponentReady = false;
        } else {
          throw new Error('User not in lobby');
        }
        await lobby.save();
        io.to(`lobby:${lobby.id}`).emit(
          'lobby:unready:success',
          success({ userId }),
        );
      },
      socket,
      'lobby:unready:error',
    ),
  );

  socket.on(
    'lobby:dotaId:set',
    catchError(
      async ({ lobbyId, dotaId }) => {
        const userId = socket?.decoded?.id;
        if (!userId) throw new NotAuthorizedError();

        const lobby = await Lobby.findByPk(lobbyId, {
          attributes: ['id', 'ownerId', 'opponentId'],
        });
        if (!lobby) throw new LobbyNotFoundError();

        if (lobby.ownerId === userId) {
          lobby.ownerDotaId = dotaId;
        } else if (lobby.opponentId === userId) {
          lobby.opponentDotaId = dotaId;
        } else {
          throw new Error('User not in lobby');
        }
        await lobby.save();

        io.to(`lobby:${lobby.id}`).emit(
          'lobby:dotaId:set:success',
          success({
            userId,
            lobbyId,
            dotaId,
          }),
        );
      },
      socket,
      'lobby:dotaId:set:error',
    ),
  );

  /**
   * Removes specified user from lobby.
   */
  socket.on(
    'lobby:kick',
    catchError(
      async ({ lobbyId, userId }) => {
        const requestedUserId = socket?.decoded?.id;
        if (!requestedUserId) throw new NotAuthorizedError();

        const lobby = await Lobby.findByPk(lobbyId, {
          attributes: ['id', 'ownerId', 'opponentId'],
        });
        if (!lobby) throw new LobbyNotFoundError();

        if (lobby.ownerId !== requestedUserId) throw new Error('Not owner');
        if (lobby.opponentId !== userId) throw new Error('Not opponent');

        lobby.opponentId = null;
        lobby.isOpponentReady = false;
        lobby.opponentDotaId = false;
        await lobby.save();

        io.to(`lobby:${lobbyId}`).emit('lobby:kicked', success({ userId }));
        io.emit('lobby:freed', success({ lobbyid }));
      },
      socket,
      'lobby:kick:error',
    ),
  );

  /**
   * Removes user from lobby. If user is the owner, then cancel lobby.
   */
  socket.on(
    'lobby:leave',
    catchError(
      async ({ lobbyId }) => {
        const userId = socket?.decoded?.id;
        if (!userId) throw new NotAuthorizedError();

        const lobby = await Lobby.findByPk(lobbyId, {
          attributes: ['id', 'ownerId', 'opponentId'],
        });
        if (!lobby) throw new LobbyNotFoundError();

        if (lobby.ownerId === userId) {
          lobby.status = 'canceled';
          await lobby.save();
          io.to(`lobby:${lobbyId}`).emit('lobby:leaved', success({ userId }));
          io.emit('lobby:canceled', success({ lobbyId }));
        } else if (lobby.opponentId === userId) {
          lobby.opponentId = null;
          lobby.isOpponentReady = false;
          await lobby.save();
          io.to(`lobby:${lobbyId}`).emit('lobby:leaved', success({ userId }));
          io.emit('lobby:freed', success({ lobbyId }));
        } else {
          throw new Error('User not in lobby');
        }
        socket.leave(`lobby:${lobbyId}`);
      },
      socket,
      'lobby:leave:error',
    ),
  );

  /**
   * Check if disconnected user hosted lobby. And if yes - removes him from
   * lobby or cancel lobby.
   */
  socket.on(
    'disconnect',
    catchError(async () => {
      const userId = socket?.decoded?.id;
      if (!userId) return;

      const lobbies = await Lobby.findAll({
        attributes: ['id', 'ownerId', 'opponentId'],
        where: {
          status: 'waiting',
          [Op.or]: [{ ownerId: userId }, { opponentId: userId }],
        },
      });
      if (lobbies.length === 0) return;

      lobbies.forEach(async lobby => {
        if (userId === lobby.ownerId) {
          lobby.status = 'canceled';
          io.emit('lobby:canceled', success({ lobbyId: lobby.id }));
        } else if (userId === lobby.opponentId) {
          lobby.opponentId = null;
          lobby.isOpponentReady = false;
          io.to(`lobby:${lobby.id}`).emit('lobby:leaved', success({ userId }));
          io.emit('lobby:freed', success({ lobbyId: lobby.id }));
        }
        await lobby.save();
      });
    }),
  );
};
