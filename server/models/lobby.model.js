const { Model, DataTypes } = require('sequelize');

module.exports = class Lobby extends Model {
  static init(sequelize) {
    return super.init(
      {
        id: {
          type: DataTypes.UUID,
          defaultValue: DataTypes.UUIDV4,
          primaryKey: true,
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        sum: {
          type: DataTypes.DOUBLE,
          defaultValue: 0,
          allowNull: false,
        },
        status: {
          type: DataTypes.ENUM('waiting', 'playing', 'done', 'canceled'),
          defaultValue: 'waiting',
          allowNull: false,
        },
        ownerDotaId: {
          type: DataTypes.STRING,
          field: 'owner_dota_id',
        },
        opponentDotaId: {
          type: DataTypes.STRING,
          field: 'opponent_dota_id',
        },
        isOwnerReady: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
          allowNull: false,
          field: 'is_owner_ready',
        },
        isOpponentReady: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
          allowNull: false,
          field: 'is_opponent_ready',
        },
        isMangoBanned: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
          allowNull: false,
          field: 'is_mango_banned',
        },
        isCloakBanned: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
          allowNull: false,
          field: 'is_cloak_banned',
        },
        isHatBanned: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
          allowNull: false,
          field: 'is_hat_banned',
        },
        isRaindropsBanned: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
          allowNull: false,
          field: 'is_raindrops_banned',
        },
        isStickBanned: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
          allowNull: false,
          field: 'is_stick_banned',
        },
        isBottleBanned: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
          allowNull: false,
          field: 'is_bottle_banned',
        },
      },
      {
        sequelize,
        modelName: 'Lobby',
        tableName: 'Lobby',
      },
    );
  }

  static associate(models) {
    const { User } = models;
    this.belongsTo(User, {
      foreignKey: 'ownerId',
      as: 'owner',
    });
    this.belongsTo(User, {
      foreignKey: 'opponentId',
      as: 'opponent',
    });
    this.belongsTo(User, {
      foreignKey: 'winnerId',
      as: 'winner',
    });
  }

  static applyHooks() {
    this.afterSync(() => {
      this.update(
        {
          status: 'canceled',
        },
        {
          where: { status: ['waiting', 'playing'] },
        },
      );
    });
  }
};
