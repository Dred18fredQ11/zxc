const { Model, DataTypes } = require('sequelize');

module.exports = class Payment extends Model {
  static init(sequelize) {
    return super.init(
      {
        unitpayId: DataTypes.STRING,
        status: DataTypes.TINYINT,
        account: DataTypes.STRING,
        description: DataTypes.STRING,
        sum: DataTypes.DOUBLE,
        payerSum: DataTypes.DOUBLE,
        currency: DataTypes.STRING,
        payerCurrency: DataTypes.STRING,
        profit: DataTypes.DOUBLE,
        completedAt: DataTypes.DATE,
        isRefund: DataTypes.BOOLEAN,
        isTest: DataTypes.BOOLEAN,
      },
      {
        sequelize,
        modelName: 'Payment',
        tableName: 'Payment',
      },
    );
  }

  static associate(models) {
    const { Lobby } = models;
    this.hasOne(Lobby, {});
  }
};
