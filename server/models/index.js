const { Sequelize } = require('sequelize');
const config = require('../config');

const sequelize = new Sequelize(
  config.db.schemaName,
  config.db.user,
  config.db.password,
  {
    dialect: 'mysql',
    logging: true,
  },
);

const models = {
  User: require('./user.model').init(sequelize),
  Lobby: require('./lobby.model').init(sequelize),
};

Object.values(models).forEach(model => {
  if (typeof model.associate === 'function') {
    model.associate(models)
  }
});

Object.values(models).forEach(model => {
  if (typeof model.applyHooks === 'function') {
    model.applyHooks()
  }
});

module.exports = {
  ...models,
  sequelize,
};
