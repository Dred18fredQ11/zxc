const { Model, DataTypes } = require('sequelize');

module.exports = class User extends Model {
  static init(sequelize) {
    return super.init(
      {
        nickname: DataTypes.STRING,
        dotaId: DataTypes.STRING,
        avatar: DataTypes.STRING,
        twitchLogin: DataTypes.STRING,
        twitchEmail: DataTypes.STRING,
        twitchId: DataTypes.STRING,
        balance: {
          type: DataTypes.DOUBLE,
          defaultValue: 0,
        },
      },
      {
        sequelize,
        modelName: 'User',
        tableName: 'User',
      },
    );
  }

  static associate(models) {
    const { Lobby } = models;
    this.hasMany(Lobby, {
      foreignKey: 'ownerId',
      as: 'lobbyListAsOwner',
    });
    this.hasMany(Lobby, {
      foreignKey: 'opponentId',
      as: 'lobbyListAsOpponent',
    });
    this.hasMany(Lobby, {
      foreignKey: 'winnerId',
      as: 'lobbyListAsWinner',
    });
  }
};
