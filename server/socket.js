module.exports = http => {
  const io = require('socket.io')(http);
  const handlers = [
    require('./handlers/user.handler'),
    require('./handlers/lobby.handler'),
  ];
  io.use(require('./middleware/auth.middleware')).on('connection', socket => {
    handlers.forEach(handler => handler(socket, io));
  });
  return io;
};
