const jwt = require('jsonwebtoken');
const secret = require('../config').app.secret;

module.exports = (socket, next) => {
  const token = socket?.handshake?.auth?.token;
  if (!token) return next();
  try {
    socket.decoded = jwt.verify(token, secret);
  } catch (e) {
    console.log(e);
  } finally {
    next();
  }
};
