class NotAuthorizedError extends Error {
  constructor() {
    super('Not authorized');
  }
}

class UserNotFoundError extends Error {
  constructor() {
    super('Such user not found');
  }
}

class LobbyNotFoundError extends Error {
  constructor() {
    super('Lobby not found');
  }
}

class LobbyIsFullError extends Error {
  constructor() {
    super('Lobby is full');
  }
}

module.exports = {
  NotAuthorizedError,
  UserNotFoundError,
  LobbyNotFoundError,
  LobbyIsFullError,
}