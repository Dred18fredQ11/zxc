require('dotenv').config();

const required = [
  'APP_PORT',
  'APP_SECRET',
  'DB_SCHEMA_NAME',
  'DB_USER',
  'TWITCH_CLIENT_ID',
  'TWITCH_SECRET',
  'TWITCH_REDIRECT_URI',
  'UNITPAY_PUBLIC_KEY',
  'UNITPAY_SECRET_KEY',
  'UNITPAY_API_SECRET_KEY',
  'UNITPAY_LOGIN',
  'UNITPAY_PROJECT_ID',
  'UNITPAY_REDIRECT_URL',
  'TELEGRAM_BOT_CHAT_ID',
  'TELEGRAM_BOT_SEND_MESSAGE_URL',
];
const errors = required.filter(envVar => !process.env[envVar]);
if (errors.length > 0) {
  throw new Error(`Require env variables: ${errors.join(', ')}`);
}

module.exports = {
  app: {
    port: process.env.APP_PORT,
    secret: process.env.APP_SECRET,
  },
  db: {
    schemaName: process.env.DB_SCHEMA_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
  },
  twitch: {
    clientId: process.env.TWITCH_CLIENT_ID,
    clientSecret: process.env.TWITCH_SECRET,
    redirectUri: process.env.TWITCH_REDIRECT_URI,
  },
  unitpay: {
    publicKey: process.env.UNITPAY_PUBLIC_KEY,
    secretKey: process.env.UNITPAY_SECRET_KEY,
    apiSecretKey: process.env.UNITPAY_API_SECRET_KEY,
    login: process.env.UNITPAY_LOGIN,
    projectId: process.env.UNITPAY_PROJECT_ID,
    redirectUrl: process.env.UNITPAY_REDIRECT_URL,
  },
  telegramBot: {
    chatId: process.env.TELEGRAM_BOT_CHAT_ID,
    sendMessageUrl: process.env.TELEGRAM_BOT_SEND_MESSAGE_URL,
  }
};
