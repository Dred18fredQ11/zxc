const next = require('next');
const bodyParser = require('body-parser');
const requestIp = require('request-ip');
const { parse } = require('url');
const { sequelize } = require('./models');
const port = require('./config').app.port;

const app = require('express')();
app.set('trust proxy', 1);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(requestIp.mw());

const http = require('http').Server(app);

// SOCKETS
const io = require('./socket')(http);
app.use((req, res, next) => {
  req.io = io;
  next();
});

// REST API
app.use('/api/auth', require('./routes/auth.routes'));

const nextApp = next({
  dev: process.env.NODE_ENV !== 'production',
  dir: './client',
});

nextApp.prepare().then(() => {
  const handler = nextApp.getRequestHandler();
  app.get('*', (req, res) => {
    const parsedUrl = parse(req.url, true);
    return handler(req, res, parsedUrl);
  });
  http.listen(port, async err => {
    if (err) throw err;
    await sequelize.authenticate();
    // await sequelize.sync({  });
    console.log(`App is listening at port: ${port}`);
  });
});
