const router = require('express').Router();
const jwt = require('jsonwebtoken');
const {
  twitchAuthUrl,
  requestToken,
  requestUserData,
} = require('../services/twitch');
const { User } = require('../models');
const { secret } = require('../config').app;

// GET /api/auth/twitch
router.get('/twitch', (req, res) => {
  res.redirect(twitchAuthUrl);
});

// GET /api/auth/twitch/callback
router.get('/twitch/callback', async (req, res) => {
  try {
    const code = req.query.code;
    if (!code) return res.redirect('/auth-error');

    const { access_token: twitchToken } = await requestToken(code);
    const userData = await requestUserData(twitchToken);
    const [user] = await User.findOrCreate({
      where: {
        twitchId: userData.id,
        twitchLogin: userData.login,
      },
      defaults: {
        twitchId: userData.id,
        twitchEmail: userData.email,
        twitchLogin: userData.login,
        nickname: userData.display_name,
        avatar: userData.profile_image_url,
      },
    });

    const jwtToken = jwt.sign(
      { id: user.id, twitchId: user.twitchId, twitchLogin: user.twitchLogin },
      secret,
    );
    res.redirect(`/auth?token=${jwtToken}`);
  } catch (e) {
    console.log(e);
    res.redirect('/auth-error');
  }
});

module.exports = router;
